CREATE TABLE autor
(
	id_autor INT NOT NULL AUTO_INCREMENT,
	nome VARCHAR(50),
	sobrenome VARCHAR(100),
	login VARCHAR(50), 
	senha VARCHAR(50), 
	ADMIN BOOLEAN,
	PRIMARY KEY(id_autor)
);

CREATE TABLE editora 
(
	id_editora INT NOT NULL AUTO_INCREMENT,
	nome VARCHAR(100),
	local VARCHAR(100),
	PRIMARY KEY(id_editora)
);

CREATE TABLE periodico
(
	id_periodico INT NOT NULL AUTO_INCREMENT,
	nome VARCHAR(100), 
	qualis VARCHAR(5),
	PRIMARY KEY(id_periodico)
);

CREATE TABLE tipo_evento
(
	id_tipo_evento INT NOT NULL AUTO_INCREMENT,
	tipo VARCHAR(50),
	PRIMARY KEY(id_tipo_evento)
);

CREATE TABLE evento
(
	id_evento INT NOT NULL AUTO_INCREMENT,
	nome VARCHAR(100), 
	qualis VARCHAR(5),
	id_tipo_evento INT,
	PRIMARY KEY(id_evento), 
	FOREIGN KEY(id_tipo_evento) REFERENCES tipo_evento(id_tipo_evento)
);

CREATE TABLE alcance
(
	id_alcance INT NOT NULL AUTO_INCREMENT,
	alcance VARCHAR(50),
	PRIMARY KEY(id_alcance)
);

CREATE TABLE natureza
(
	id_natureza INT NOT NULL AUTO_INCREMENT,
	natureza VARCHAR(50),
	PRIMARY KEY(id_natureza)
);

CREATE TABLE pub_livro 
(
	id_pub INT NOT NULL AUTO_INCREMENT,
	titulo VARCHAR(100), 
	id_alcance INT,
	id_natureza INT, 
	id_editora INT NOT NULL,
	local VARCHAR(100), 
	num_pags INT,
	ano INT,
	colecao_serie VARCHAR(80),
	edicao VARCHAR(10),  
	PRIMARY KEY(id_pub),
	FOREIGN KEY(id_editora) REFERENCES editora(id_editora),
	FOREIGN KEY(id_alcance) REFERENCES alcance(id_alcance),
	FOREIGN KEY(id_natureza) REFERENCES natureza(id_natureza) 
);

CREATE TABLE pub_periodico
(
	id_pub INT NOT NULL AUTO_INCREMENT,
	titulo VARCHAR(100), 
	id_alcance INT,
	id_natureza INT, 
	id_editora INT NOT NULL,
	local VARCHAR(100), 
	num_pags INT,
	ano INT,
	pag_inicial INT,
	pag_final INT,
	mes VARCHAR(10), 
	volume INT,
	fasciculo INT,
	PRIMARY KEY(id_pub),
	FOREIGN KEY(id_editora) REFERENCES editora(id_editora),
	FOREIGN KEY(id_alcance) REFERENCES alcance(id_alcance),
	FOREIGN KEY(id_natureza) REFERENCES natureza(id_natureza)
);

/*tabela relacionando pub em periodico com o periodico pois: 
um periodico pode ter mais de uma publicacao e uma publicacao pode estar em mais de um periodico*/
CREATE TABLE rel_pub_periodico
(
	id_pub INT AUTO_INCREMENT,
	id_periodico INT,
	FOREIGN KEY(id_pub) REFERENCES pub_periodico(id_pub),
	FOREIGN KEY(id_periodico) REFERENCES periodico(id_periodico)
);

CREATE TABLE pub_evento
(
	id_pub INT NOT NULL AUTO_INCREMENT,
	titulo VARCHAR(100), 
	id_alcance INT,
	id_natureza INT, 
	id_editora INT NOT NULL,
	local VARCHAR(100), 
	num_pags INT,
	ano INT,
	num_evento INT,
	local_evento VARCHAR(100), 
	ano_evento INT,
	PRIMARY KEY(id_pub),
	FOREIGN KEY(id_editora) REFERENCES editora(id_editora),
	FOREIGN KEY(id_alcance) REFERENCES alcance(id_alcance),
	FOREIGN KEY(id_natureza) REFERENCES natureza(id_natureza)
);

/*tabela relacionando pub em periodico com o evento pois: 
um evento pode ter mais de uma publicacao e uma publicacao pode estar em mais de um evento*/
CREATE TABLE rel_pub_evento
(
	id_pub INT AUTO_INCREMENT,
	id_evento INT,
	FOREIGN KEY(id_pub) REFERENCES pub_evento(id_pub),
	FOREIGN KEY(id_evento) REFERENCES evento(id_evento)
);

CREATE TABLE pub_capitulo
(
	id_pub INT NOT NULL AUTO_INCREMENT,
	titulo VARCHAR(100), 
	id_alcance INT,
	id_natureza INT, 
	id_editora INT NOT NULL,
	local VARCHAR(100), 
	num_pags INT,
	ano INT,
	titulo_livro VARCHAR(100),
	pag_inicial INT,
	pag_final INT,
	PRIMARY KEY(id_pub),
	FOREIGN KEY(id_editora) REFERENCES editora(id_editora),
	FOREIGN KEY(id_alcance) REFERENCES alcance(id_alcance),
	FOREIGN KEY(id_natureza) REFERENCES natureza(id_natureza)
);

/* uma publicação de capítulo pode estar em um livro que possui mais de um autor, 
esses autores nao necessariamente estao no db, pois o que esta em questao
é o capitulo do livro não o livro em si. tabela necessaria pelo fato de poder ter mais de um autor no campo "autor do livro". */
CREATE TABLE autor_livro_pub
(
	id_pub INT NOT NULL AUTO_INCREMENT, 
	nome VARCHAR(100),
	sobrenome VARCHAR(100),
	PRIMARY KEY(id_pub),
	FOREIGN KEY(id_pub) REFERENCES pub_capitulo(id_pub)
);
