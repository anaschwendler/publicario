<?php

class Publicacoes extends CI_Controller {

	public function view()
	{

		$data['title'] = 'Publicações';

		if (!$this->session->userdata('logged_in')) {
			//prompt users that there is no session
			$data["usuario_logado"] = "";
		} else {
			//get the session[user_input]
			$session_data = $this->session->userdata('logged_in');
			$data["usuario_logado"] = $session_data['nome_usuario'];
		}

		$this->load->view('templates/header', $data);
		$this->load->view('publicacoes/view');
		$this->load->view('templates/footer');
	}

	/*
	Criando publicação:
	- Campo que vem abertos: Dropdown do tipo.

	- Selecionado o tipo, abre os campos correspondentes ao tipo.

	- Cadastra-se com o sql normal.

	Autores:
	- Dropdown com os autores cadastrados.
	- Caso não exista, botão para cadastrar: nome, sobrenome e email. 
	(caso o autor cadastrado entre no sistema, é solicitado apenas o login e a senha.)
	*/

	
}