<?php
class Users extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('users_model');
	}

	public function index()
	{

		//$data['users'] = $this->users_model->get_users();
	}

	public function view($slug)
	{
		//$data['users'] = $this->users_model->get_users($slug);
	}

	public function create()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$data['title'] = 'Cadastro Usuário';
		
		if (!$this->session->userdata('logged_in')) {
			$data["usuario_logado"] = "";
		} else {
			$session_data = $this->session->userdata('logged_in');
			$data["usuario_logado"] = $session_data['nome_usuario'];
		}

		$this->form_validation->set_rules('nome_usuario', 'Nome', 'required');
		$this->form_validation->set_rules('sobrenome_usuario', 'Sobrenome', 'required');
		$this->form_validation->set_rules('email_usuario', 'Email', 'required');
		$this->form_validation->set_rules('username_usuario', 'Usuario', 'required');
		$this->form_validation->set_rules('senha_usuario', 'Senha', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header', $data);
			$this->load->view('users/create');
			$this->load->view('templates/footer');

		}
		else
		{
			$resultado = $this->users_model->set_user();
			if($resultado == 0) //login já existente
			{
				$data['nome'] = "Login já existente, tente novamente.";

				$this->load->view('templates/header', $data);
				$this->load->view('users/create', $data);
				$this->load->view('templates/footer');
			}
			else if($resultado == 1)//email já cadastrado
			{
				$data['nome'] = "Email já cadastrado. Crie um login";
				//volta pro create, com o nome, sobrenome e email já preenchido

				$this->load->view('templates/header', $data);
				$this->load->view('users/create', $data);
				$this->load->view('templates/footer');
			}
			else 
			{
				$this->load->view('templates/header', $data);
				$this->load->view('users/success');
				$this->load->view('templates/footer');
			}			
		}
	}

	public function login()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		/*Parei aqui, em validar o login*/

		$data['title'] = 'Login Usuário';

		if (!$this->session->userdata('logged_in')) {
			//prompt users that there is no session
			$data["usuario_logado"] = "";
		} else {
			//get the session[user_input]
			$session_data = $this->session->userdata('logged_in');
			$data["usuario_logado"] = $session_data['nome_usuario'];
		}

		$this->form_validation->set_rules('username_usuario', 'Usuario', 'required');
		$this->form_validation->set_rules('senha_usuario', 'Senha', 'required');


		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header', $data);
			$this->load->view('users/login', $data);
			$this->load->view('templates/footer');

		}
		else
		{
			$username = $this->input->post('username_usuario');
			$password = $this->input->post('senha_usuario');

			$result = $this->users_model->login($username, $password);

			if($result)
			{
				$session_usuario = array();
				foreach ($result as $row) {
					$session_usuario = array(
						'id' => $row->id_autor,
						'nome_usuario' => $row->nome 
					);
					$this->session->set_userdata('logged_in', $session_usuario);
				}

				if($this->session->userdata('logged_in')){
					$session_data = $this->session->userdata('logged_in');
					$data['usuario_logado'] = $session_data['nome_usuario'];

					$this->load->view('templates/header', $data);
					$this->load->view('publicacoes/view', $data);
					$this->load->view('templates/footer');
				}

			}
			else
			{
				$this->form_validation->set_message('check_database', 'Invalid username or password');
				return false;
			}
		}
	}

	function logout()
 	{
 		$data['title'] = 'Logout';
		
	   	$this->session->unset_userdata('logged_in');

	   	if (!$this->session->userdata('logged_in')) {
			//prompt users that there is no session
			$data["usuario_logado"] = "";
		} else {
			//get the session[user_input]
			$session_data = $this->session->userdata('logged_in');
			$data["usuario_logado"] = $session_data['nome_usuario'];
		}

	   	$this->load->view('templates/header', $data);
		$this->load->view('users/logout', $data);
		$this->load->view('templates/footer');
 	}
}

?>