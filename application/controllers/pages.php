<?php

class Pages extends CI_Controller {

	public function view($page = 'home')
	{

		if ( ! file_exists(APPPATH.'/views/pages/'.$page.'.php'))
		{
			// Whoops, we don't have a page for that!
			show_404();
		}

		$data['title'] = ucfirst($page); // Capitalize the first letter

		if (!$this->session->userdata('logged_in')) {
			//prompt users that there is no session
			$data["usuario_logado"] = "";
		} else {
			//get the session[user_input]
			$session_data = $this->session->userdata('logged_in');
			$data["usuario_logado"] = $session_data['nome_usuario'];
		}

		$this->load->view('templates/header', $data);
		$this->load->view('pages/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}
}