<?php
class Users_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	public function login($username, $password)
	{
		$this->db->select('id_autor, nome');
		$this->db->from('autor');
		$this->db->where('login', $username);
		$this->db->where('senha', MD5($password));
		$this->db->limit(1);

		$query = $this->db->get();


		//die(var_dump($query));

		if($query->num_rows() == 1)
	   	{
	    	return $query->result();
	   	}
	   	else
	    {
	    	return false;
	    }

	}

	public function set_user()
	{
		$this->load->helper('url');

		$data = array(
			'nome' => $this->input->post('nome_usuario'),
			'sobrenome' => $this->input->post('sobrenome_usuario'),
			'email' => $this->input->post('email_usuario'),
			'login' => $this->input->post('username_usuario'),
			'senha' => MD5($this->input->post('senha_usuario')),
			'ADMIN' => 0
		);

		$this->db->select('login');
		$this->db->from('autor');
		$this->db->where('login', $data['login']);

		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return 0;
			//return "treta, login já existe";
		}

		$this->db->select('email');
		$this->db->from('autor');
		$this->db->where('email', $data['email']);

		$query = $this->db->get();

		if($query->num_rows() > 0)
		{
			return 1;
			//return "treta, e já existe";
		}


		return $this->db->insert('autor', $data);
	}
}