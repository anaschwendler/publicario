
<div class="container login">
	<?php echo validation_errors(); ?>
		<?php $attributes = array('class' => 'form-signin', 'role' => 'form'); ?>
		<?php echo form_open('users/create', $attributes) ?>
		<h2>Cadastrar Usuário</h2>
		<div class="form-group">
			<input type="text" class="form-control" placeholder="Nome" name="nome_usuario" required />
		</div>
		<div class="form-group">
			<input type="text" class="form-control" placeholder="Sobrenome" name="sobrenome_usuario" required />
		</div>
		<div class="form-group">
			<input type="email" class="form-control" placeholder="E-mail" name="email_usuario" required />
		</div>
		<div class="form-group">
			<input type="text" class="form-control" placeholder="Usuario" name="username_usuario" required />
		</div>
		<div class="form-group">
			<input type="password" class="form-control" placeholder="Senha" name="senha_usuario" required />
		</div>

		<button class="btn btn-lg btn-primary btn-block" name="submit" type="submit">Cadastrar</button>
	</form>
</div>