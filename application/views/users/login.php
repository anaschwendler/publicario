<div class="container login">
	<?php echo validation_errors(); ?>
		<?php $attributes = array('class' => 'form-signin', 'role' => 'form'); ?>
		<?php echo form_open('users/login', $attributes) ?>
		<h2>Login</h2>

		<div class="form-group">
			<input type="text" class="form-control" placeholder="Usuario" name="username_usuario" required />
		</div>
		<div class="form-group">
			<input type="password" class="form-control" placeholder="Senha" name="senha_usuario" required />
		</div>

		<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
	</form>
</div>