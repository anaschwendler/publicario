<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="shortcut icon" type="image/png" href="assets/favicon.png"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href='http://fonts.googleapis.com/css?family=Lobster|Roboto' rel='stylesheet' type='text/css'>
	<title><?php echo $title ?> - Publicário</title>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/bootstrap/3.3.0/css/bootstrap.min.css">
	<!--<link rel="stylesheet" href="<?php //echo base_url('styles/css/bootstrap.min.css'); ?>" type="text/css">-->
    <link rel="stylesheet" href="<?php echo base_url('styles/css/style.css'); ?>" >
</head>
<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>	
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/publicario">Publicário</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-right">
					<li>
						<a href="about">Sobre</a>
					</li>
					<?php if($usuario_logado == '') {?>
						<li>
							<button type="button" class="btn btn-default navbar-btn"><a href="login">Login</a></button>
						</li>
						<li>
							<button type="button" class="btn btn-default navbar-btn"><a href="cadastrar">Cadastre-se</a></button>
						</li>
					<?php } 
					else {?>
						<li>
							<a href="inicio">Publicações</a>
						</li>
						<li>
							<a href="inicio">Bem-vindo, <?php echo $usuario_logado;?></a>
						</li>
						<li>
							<a href="users/logout">Logout</a>
						</li>
					<?php }?>
				</ul>
				<form class="navbar-form navbar-right">
					<input type="text" class="form-control" placeholder="Pesquisar...">
				</form>
			</div>
		</div>	
	</nav>