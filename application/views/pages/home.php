<div class="container">
	<h1 class="page-header"> Últimas publicações</h1>
	<div class="table-responsive">
		<table class="table table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>Tipo</th>
					<th>Referência</th>
					<th>Ano</th>
					<th>Natureza</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>#1</td>
					<td>Tipo 1</td>
					<td>Referência 1</td>
					<td>Ano 2001</td>
					<td>Natureza 1</td>
				</tr>
				<tr>
					<td>#2</td>
					<td>Tipo 2</td>
					<td>Referência 2</td>
					<td>Ano 2002</td>
					<td>Natureza 2</td>
				</tr>
				<tr>
					<td>#3</td>
					<td>Tipo 3</td>
					<td>Referência 3</td>
					<td>Ano 2003</td>
					<td>Natureza 3</td>
				</tr>
				<tr>
					<td>#4</td>
					<td>Tipo 4</td>
					<td>Referência 4</td>
					<td>Ano 2004</td>
					<td>Natureza 4</td>
				</tr>
			</tbody>
		</table>
	</div>
	

	<h2 class="sub-header">Visualizações</h2>
	<div class="row">
		<div class="row">
        <div class="col-lg-4">
          <img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" style="width: 100px; height: 100px;">
          <h2>Adicione sua referência</h2>
          <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna.</p>
          <p><a class="btn btn-default" href="#" role="button">Cadastre »</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" style="width: 100px; height: 100px;">
          <h2>Ordene por ano</h2>
          <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh.</p>
          <p><a class="btn btn-default" href="#" role="button">Ordenar »</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="img-circle" src="data:image/gif;base64,R0lGODlhAQABAIAAAHd3dwAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==" alt="Generic placeholder image" style="width: 100px; height: 100px;">
          <h2>Ordene por tipo</h2>
          <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
          <p><a class="btn btn-default" href="#" role="button">Ordenar »</a></p>
        </div><!-- /.col-lg-4 -->
      </div>
	</div>

</div>